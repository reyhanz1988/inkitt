/* eslint-disable jest/expect-expect */
/* eslint-disable jest/no-commented-out-tests */
/* eslint-disable no-undef */

beforeAll(async () => {
	await device.launchApp();
});
const testFn = () => {
	//Login screen already test rendered after switch language 

	it("LoginScreen case fail login test", async () => {
		await element(by.id("emailInput")).typeText("reyhanz1988\n");
		await element(by.id("passwordInput")).typeText("adm\n");
		await element(by.id("loginButton")).tap();
		await expect(element(by.id("loginError"))).toBeVisible();
	});
	it("LoginScreen case success login test", async () => {
		await element(by.id("emailInput")).typeText("@gmail.com\n");
		await element(by.id("passwordInput")).typeText("in\n");
		await element(by.id("loginButton")).tap();
		await expect(element(by.id("HomeScreen"))).toBeVisible();
	});
	//MENU
	it("Render MenuScreen test", async () => {
		await element(by.id("MenuNav")).tap();
		await expect(element(by.id("MenuScreen"))).toBeVisible();
	});
	//TEST BACK TO LOGOUT
	it("Render LoginScreen test", async () => {
		await element(by.id("LogoutButton")).tap();
		await expect(element(by.id("LoginScreen"))).toBeVisible();
	});
};
describe("Paack App En", () => {
	it("Switch Languages to En test", async () => {
		await element(by.id("LangOptions_en")).tap();
		await expect(element(by.id("LoginScreen"))).toBeVisible();
	});
	testFn();
});
describe("Paack App De", () => {
	it("Switch Languages to De test", async () => {
		await element(by.id("LangOptions_de")).tap();
		await expect(element(by.id("LoginScreen"))).toBeVisible();
	});
	testFn();
});
