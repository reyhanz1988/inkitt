import React, { useContext } from "react";
import { LangContext } from "../../App";
import { StyleSheet, View } from "react-native";
import { Appbar } from "react-native-paper";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import NewScreen from "./NewScreen";
import PopularScreen from "./PopularScreen";
import CommunityScreen from "./CommunityScreen";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Discover";
import PropTypes from "prop-types";

const Tab = createMaterialTopTabNavigator();

const DiscoverScreen = () => {
	const { currentLang } = useContext(LangContext);
	//RENDER
	return (
		<View style={styles.wrapper} testID="DiscoverScreen">
			<Appbar.Header>
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[0][currentLang]}
				/>
				<LangComponent />
			</Appbar.Header>
			<Tab.Navigator
				initialRouteName="New"
				screenOptions={{
					tabBarLabelStyle: {
						fontSize: 16,
						color: "#000",
						textTransform: "none",
					},
				}}>
				<Tab.Screen
					name="New"
					component={NewScreen}
					options={{
						tabBarTestID: "NewNav",
						tabBarLabel: languages[1][currentLang],
					}}
				/>
				<Tab.Screen
					name="Popular"
					component={PopularScreen}
					options={{
						tabBarTestID: "PopularNav",
						tabBarLabel: languages[2][currentLang],
					}}
				/>
				<Tab.Screen
					name="Community"
					component={CommunityScreen}
					options={{
						tabBarTestID: "CommunityNav",
						tabBarLabel: languages[3][currentLang],
					}}
				/>
			</Tab.Navigator>
		</View>
	);
};

DiscoverScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type DiscoverScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default DiscoverScreen;
