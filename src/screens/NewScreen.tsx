import React, { useContext } from "react";
import { LangContext } from "../../App";
import { StyleSheet, View, Text } from "react-native";
import languages from "../screensTranslations/New";
import PropTypes from "prop-types";


const NewScreen = () => {
	const { currentLang } = useContext(LangContext);
	//RENDER
	return (
		<View style={styles.wrapper} testID="NewScreen">
			<Text>{languages[0][currentLang]}</Text>
		</View>
	);
};

NewScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type NewScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default NewScreen;
