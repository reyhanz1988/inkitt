import React, { useContext } from "react";
import { LangContext } from "../../App";
import { StyleSheet, View, Text } from "react-native";
import languages from "../screensTranslations/Popular";
import PropTypes from "prop-types";


const PopularScreen = () => {
	const { currentLang } = useContext(LangContext);
	//RENDER
	return (
		<View style={styles.wrapper} testID="PopularScreen">
			<Text>{languages[0][currentLang]}</Text>
		</View>
	);
};

PopularScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type PopularScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default PopularScreen;
