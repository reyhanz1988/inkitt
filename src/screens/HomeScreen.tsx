import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../../App";
import { connect } from "react-redux";
import * as accountActions from "../redux/actions/accountActions";
import * as productActions from "../redux/actions/productActions";
import { Image, StyleSheet, ScrollView, View } from "react-native";
import { ActivityIndicator, Appbar, Button, Card, List, Divider } from "react-native-paper";
import LangComponent from "../components/LangComponent";
import languages from "../screensTranslations/Home";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import PropTypes from "prop-types";

const HomeScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [loading, setLoading] = useState(true);
	const [getDataError, setGetDataError] = useState([]);
	const [user, setUser] = useState([]);
	const [books, setBooks] = useState([]);
	useEffect(() => {
		setLoading(true);
		props.getUser();
		props.getBooks();
	}, []);
	//PROPS UPDATE USER
	useEffect(() => {
		setUser(props.getUserRes);
		setLoading(false);
	}, [props.getUserRes]);
	//PROPS UPDATE BOOKS
	useEffect(() => {
		setBooks(props.getBooksRes);
		setGetDataError("");
		setLoading(false);
	}, [props.getBooksRes]);
	if (loading) {
		//LOADING
		return (
			<View style={styles.wrapper} testID="HomeLoading">
				<ActivityIndicator size="large" />
			</View>
		);
	} else if (getDataError) {
		//ERROR HANDLING
		return (
			<View style={styles.wrapper} testID="HomeError">
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		//RENDER
		let userData = [];
		if (user) {
			userData = (
				<Card style={styles.userCard}>
					<Card.Content>
						<List.Item
							titleStyle={{ color: SECOND_COLOR, fontWeight: "bold" }}
							title={user.name}
							descriptionStyle={{ color: SECOND_COLOR }}
							description={languages[1][currentLang]}
						/>
					</Card.Content>
				</Card>
			);
		}
		const booksList = [];
		if (books.length > 0) {
			for (const i in books) {
				let genres = "";
				if (books[i].genres.length > 0) {
					for (const j in books[i].genres) {
						genres += " • " + books[i].genres[j][currentLang];
					}
				}
				booksList.push(
					<Card key={"books_" + i} style={styles.booksCard}>
						<Card.Title title={(parseInt(i) + 1).toString() + ". " + books[i].name} />
						<Divider style={styles.theDivider} />
						<Card.Content>
							<List.Item
								title={languages[2][currentLang] + " " + books[i].author}
								description={
									books[i].chapter + " " + languages[3][currentLang] + genres
								}
								left={() => (
									<Image style={styles.bookCover} source={books[i].cover} />
								)}
								right={(props) => <List.Icon {...props} icon={"dots-vertical"} />}
								style={{
									borderBottomWidth: 1,
									borderBottomColor: "#ddd",
								}}
							/>
						</Card.Content>
						<Card.Actions style={styles.bookActions}>
							<Button
								uppercase={false}
								style={styles.actionButtons}
								mode="contained"
								onPress={() => {
									console.log("test");
								}}>
								{languages[4][currentLang]}
							</Button>
						</Card.Actions>
					</Card>,
				);
			}
		}
		return (
			<View style={styles.wrapper} testID="HomeScreen">
				<Appbar.Header>
					<Appbar.Content
						titleStyle={{ color: "#fff", textAlign: "center" }}
						title={languages[0][currentLang]}
					/>
					<LangComponent />
				</Appbar.Header>
				<View style={styles.userView}>{userData}</View>
				<ScrollView style={styles.scrollWrapper}>{booksList}</ScrollView>
			</View>
		);
	}
};

HomeScreen.propTypes = {
	getUser: PropTypes.func,
	getUserRes: PropTypes.any,
	getBooks: PropTypes.func,
	getBooksRes: PropTypes.array,
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type HomeScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	userView: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
	},
	userCard: {
		backgroundColor: FIRST_COLOR,
	},
	scrollWrapper: {
		marginBottom: 20,
	},
	booksCard: {
		margin: 20,
	},
	theDivider: {
		backgroundColor: "#a6d3cc",
		height: 2,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	bookCover: {
		resizeMode: "contain",
		width: 40,
		height: 80,
	},
	bookActions: {
		justifyContent: "flex-end",
	},
	actionButtons: {
		flex: 1,
		margin: 5,
	},
});

function mapStateToProps(props) {
	return {
		getUserRes: props.accountReducer.getUserRes,
		getBooksRes: props.productReducer.getBooksRes,
	};
}
const mapDispatchToProps = {
	...accountActions,
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
