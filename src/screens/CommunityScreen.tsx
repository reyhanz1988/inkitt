import React, { useContext } from "react";
import { LangContext } from "../../App";
import { StyleSheet, View, Text } from "react-native";
import languages from "../screensTranslations/Community";
import PropTypes from "prop-types";


const CommunityScreen = () => {
	const { currentLang } = useContext(LangContext);
	//RENDER
	return (
		<View style={styles.wrapper} testID="CommunityScreen">
			<Text>{languages[0][currentLang]}</Text>
		</View>
	);
};

CommunityScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};
type CommunityScreen = PropTypes.InferProps<typeof propTypes>;

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default CommunityScreen;
