import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "../screens/HomeScreen";
import DetailsScreen from "../screens/DetailsScreen";
import DiscoverScreen from "../screens/DiscoverScreen";
import NewScreen from "../screens/NewScreen";
import PopularScreen from "../screens/PopularScreen";
import CommunityScreen from "../screens/CommunityScreen";
import MenuScreen from "../screens/MenuScreen";

const Stack = createStackNavigator();

const HomeNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Home" component={HomeScreen} />
			<Stack.Screen name="Details" component={DetailsScreen} />
		</Stack.Navigator>
	);
};
export { HomeNav };

const DiscoverNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Discover" component={DiscoverScreen} />
			<Stack.Screen name="New" component={NewScreen} />
			<Stack.Screen name="Popular" component={PopularScreen} />
			<Stack.Screen name="Community" component={CommunityScreen} />
		</Stack.Navigator>
	);
};
export { DiscoverNav };

const MenuNav = () => {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name="Menu" component={MenuScreen} />
		</Stack.Navigator>
	);
};
export { MenuNav };
