module.exports = [
	/*0*/
	{
		en: "Menu",
		de: "Speisekarte",
	},

	/*1*/
	{
		en: "Profile",
		de: "Profil",
	},

	/*2*/
	{
		en: "FAQ",
		de: "FAQ",
	},

	/*3*/
	{
		en: "Privacy Policy",
		de: "Datenschutz-Bestimmungen",
	},

	/*4*/
	{
		en: "Logout",
		de: "Ausloggen",
	},
];
