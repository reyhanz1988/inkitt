module.exports = [
	/*0*/
	{
		en: "Discover",
		de: "Entdecken",
	},

	/*1*/
	{
		en: "New",
		de: "Neu",
	},

	/*2*/
	{
		en: "Popular",
		de: "Beliebt",
	},

	/*3*/
	{
		en: "Community",
		de: "Gemeinschaft",
	},
];
