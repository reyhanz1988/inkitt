module.exports = [
	/*0*/
	{
		en: "Packet Details",
		de: "Detalles del paquete",
	},

	/*1*/
	{
		en: "Deliver",
		de: "Entregar",
	},

    /*2*/
	{
		en: "Due Date : ",
		de: "Fecha de vencimiento : ",
	},
];
