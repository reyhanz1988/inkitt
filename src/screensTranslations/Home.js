module.exports = [
	/*0*/
	{
		en: "Home",
		de: "Heim",
	},

	/*1*/
	{
		en: "Hello, welcome to Inkitt. Below is your current reads.",
		de: "Hallo, willkommen bei Inkitt. Unten finden Sie Ihre aktuellen Lesevorgänge.",
	},

	/*2*/
	{
		en: "By",
		de: "Durch",
	},

	/*3*/
	{
		en: "Chapters",
		de: "Kapitel",
	},

	/*4*/
	{
		en: "Read",
		de: "Lesen",
	},
];
