import ProductApi from "../api/ProductApi";
import {
	createAsyncActionType,
	//errorToastMessage,
} from "../../components/util";

// getBooks
export const GET_BOOKS = createAsyncActionType("GET_BOOKS");
export const getBooks = () => (dispatch) => {
	dispatch({
		type: GET_BOOKS.REQUESTED,
	});
	const response = ProductApi.getBooks();
	dispatch({
		type: GET_BOOKS.SUCCESS,
		payload: { response },
	});
	/*ProductApi.getBooks()
		.then((response) => {
			dispatch({
				type: GET_BOOKS.SUCCESS,
				payload: { response },
			});
		})
		.catch((error) => {
			dispatch({
				type: GET_BOOKS.ERROR,
				payload: { error },
			});
			errorToastMessage();
		});*/
};