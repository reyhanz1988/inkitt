const Books = [
	{
		id: 1,
		cover: require("../../assets/images/books/1.jpg"),
		name: "Assassins's Mark",
		author: "Natalie Le Roux",
		chapter: "12",
		genres: [
			{
				en:"Fantasy",
				de:"Fantasie",
			},
			{
				en:"Thriller",
				de:"Thriller",
			},
		],
	},
	{
		id: 2,
		cover: require("../../assets/images/books/2.jpg"),
		name: "The Only One For Me",
		author: "Jennie",
		chapter: "23",
		genres: [
			{
				en:"Romance",
				de:"Romantik",
			},
			{
				en:"Fantasy",
				de:"Fantasie",
			},
		],
	},
	{
		id: 3,
		cover: require("../../assets/images/books/3.jpg"),
		name: "The Forbidden Werewolf",
		author: "louisevarmstrong",
		chapter: "20",
		genres: [
			{
				en:"Fantasy",
				de:"Fantasie",
			},
			{
				en:"Romance",
				de:"Romantik",
			},
		],
	},
];

const User = {
	id: 1,
	photo: require("../../assets/images/rey.jpg"),
	name: "Reyhan Emir Affandie",
	nick: "Reyhan",
	email: "reyhanz1988@gmail.com",
	phone: "938 20 02 51",
	gender: "Male",
	dob: "1988-06-17",
	ssn: "847-10-9194",
	address: "Carrer de la Creu, 16",
	city: "Balsareny",
	province: "Barcelona",
	country: "Spain",
	zipcode: "08660",
	vtype: "vans",
	vplate: "BG70AYA",
};

export { Books, User };
