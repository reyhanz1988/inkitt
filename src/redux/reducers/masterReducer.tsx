import { GET_LANG, POST_LANG } from "../actions/masterActions";

const initialState = {
	successMsg: "",
	errorMsg: "",
	getLangRes: [],
	postLangRes: [],
};

const masterReducer = (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_LANG                                                                                                                                */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_LANG.REQUESTED:
			return { ...state };
		case GET_LANG.SUCCESS:
			return {
				...state,
				getLangRes: payload.response,
				errorMsg: "",
			};
		case GET_LANG.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  POST_LANG                                                                                                                               */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case POST_LANG.SUCCESS:
			return {
				...state,
				postLangRes: payload,
				errorMsg: "",
			};

		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		default:
			return state;
	}
};

export default masterReducer;
