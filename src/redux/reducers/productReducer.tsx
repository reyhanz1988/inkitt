import { GET_BOOKS } from "../actions/productActions";

const initialState = {
	successMsg: "",
	errorMsg: "",
	getBooksRes: [],
};

const productReducer = (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		/*  GET_BOOKS                                                                                                                             */
		/*------------------------------------------------------------------------------------------------------------------------------------------*/
		case GET_BOOKS.REQUESTED:
			return { ...state };
		case GET_BOOKS.SUCCESS:
			return {
				...state,
				getBooksRes: payload.response,
				errorMsg: "",
			};
		case GET_BOOKS.ERROR:
			return { ...state, errorMsg: payload.error };

		/*------------------------------------------------------------------------------------------------------------------------------------------*/

		default:
			return state;
	}
};

export default productReducer;
